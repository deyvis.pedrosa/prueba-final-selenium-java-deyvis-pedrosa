package scripts;

import dataProviders.ProductsToCompare;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.ProductSearchPage;

import java.util.concurrent.TimeUnit;

import static org.testng.Assert.fail;

public class compareProducts {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        System.setProperty("webdriver.chrome.driver","drivers/chromedriver.exe" );
        driver = new ChromeDriver();
        baseUrl = "http://magento-demo.lexiconn.com/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }
    @Test(dataProvider = "productsToCompare", dataProviderClass = ProductsToCompare.class)
    public void test(String product1, String product2) throws Exception {
        driver.get("http://magento-demo.lexiconn.com/");
        HomePage homePage = new HomePage(driver);
        ProductSearchPage productSearchPage = homePage.search(product1);
        productSearchPage.addToCompare();
        productSearchPage.search(product2);
        productSearchPage.addToCompare();
        productSearchPage.validateCompareHeader();
        productSearchPage.validateCompareButton();
        productSearchPage.productsToCompare(product1,product2);

    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }
}
