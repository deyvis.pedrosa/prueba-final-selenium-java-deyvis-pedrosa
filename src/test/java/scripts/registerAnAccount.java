package scripts;

import dataProviders.NewUserCredentials;
import dataProviders.ProductsData;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.AccountPage;
import pages.CreateAccountPage;
import pages.HomePage;
import pages.LoginPage;

import java.util.concurrent.TimeUnit;

import static org.testng.Assert.fail;

public class registerAnAccount {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        System.setProperty("webdriver.chrome.driver","drivers/chromedriver.exe" );
        driver = new ChromeDriver();
        baseUrl = "http://magento-demo.lexiconn.com/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Test(dataProvider = "credentials", dataProviderClass = NewUserCredentials.class)
    public void test(String firstname ,String lastname ,String email,String password) throws Exception {
        driver.get("http://magento-demo.lexiconn.com/");
        HomePage homePage = new HomePage(driver);
        LoginPage loginPage = homePage.selectLogin();
        loginPage.validateLoginHeader();
        loginPage.validateCreateAccountButton();
        CreateAccountPage createAnAccount = loginPage.setCreateAnAccount();
        createAnAccount.validateCreateAccountHeader();
        createAnAccount.validateInstructions();
        AccountPage accountPage = createAnAccount.setInformationInputs(firstname, lastname, email, password);
        accountPage.validateRegistration(firstname,lastname);
        accountPage.selectLogout();
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

}
