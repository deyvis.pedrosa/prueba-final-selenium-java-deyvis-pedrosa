
package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


import static org.testng.Assert.assertTrue;

public class ProductSearchPage {
    private static WebDriver driver;

    public ProductSearchPage(WebDriver driver){
        ProductSearchPage.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(id = "search")
    WebElement searchBar;

    @FindBy(css = "button[title=Compare]")
    WebElement compareButton;

    @FindBy(css = ".block-compare > div.block-title")
    WebElement productsToCompare;

    @FindBy (css = "#compare-items > li.item.odd > p > a")
    WebElement firstProductCompared;

    @FindBy (css = "#compare-items > li.item.last.even > p > a")
    WebElement secondProductCompared;

    public ProductPage clickProduct(String product){
        WebElement productLink = driver.findElement(By.linkText(product));
        productLink.click();
        return new ProductPage(driver);
    }

    @Step("Buscar producto")
    public void search(String product){
        searchBar.clear();
        searchBar.sendKeys(product);
        searchBar.sendKeys(Keys.ENTER);
    }

    @Step("Agregar a Compare")
    public void addToCompare() {
        WebElement compare_btn = driver.findElement(By.cssSelector("ul.add-to-links > li > a.link-compare"));
        compare_btn.click();
    }

    public String getComparisionLink(){
        String compareLink = compareButton.getAttribute("onclick");
        compareLink = compareLink.substring(6,compareLink.length()-2);
        return compareLink;
    }

    @Step("Verificar productos en comparación")
    public void productsToCompare( String product1, String product2) {
        String firstProductText = firstProductCompared.getText();
        assertTrue(firstProductText.contains(product1));
        String secondProductText = secondProductCompared.getText();
        assertTrue(secondProductText.contains(product2));
    }

    @Step("Validar Compare Products Header")
    public void validateCompareHeader(){
        productsToCompare.isDisplayed();
    }

    @Step("Validar Compare button")
    public void validateCompareButton() {
        compareButton.isDisplayed();
    }
}