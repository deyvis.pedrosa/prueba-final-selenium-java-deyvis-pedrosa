package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class HomePage {
    private static WebDriver driver;

    public HomePage(WebDriver driver) {
        HomePage.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "search")
    WebElement searchBar;

    @FindBy(css = "a[href*=\"header-account\"]")
    WebElement headerAccount;

    @Step("Elegir idioma")
    public void setLanguage(String languageParam) {
        Select language = new Select (driver.findElement(By.id ("select-language")));
        language.selectByVisibleText(languageParam);
    }

    @Step("Buscar producto")
    public ProductSearchPage search(String product){
        searchBar.sendKeys(product);
        searchBar.sendKeys(Keys.ENTER);
        return new ProductSearchPage(driver);
    }

    @Step("Hacer Login")
    public LoginPage selectLogin() {
        headerAccount.click();
        WebElement login_btn = driver.findElement(By.cssSelector("#header-account > div > ul > li.last > a"));
        login_btn.click();
        return  new LoginPage(driver);
    }
}
