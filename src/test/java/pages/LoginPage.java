package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
    public static WebDriver driver;

    public LoginPage(WebDriver driver){
        LoginPage.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(name = "login[username]")
    WebElement email;

    @FindBy(name = "login[password]")
    WebElement password;

    @FindBy(css = "p.required")
    WebElement requiredFields;

    @FindBy(css = ".page-title > h1")
    WebElement loginHeader;

    @FindBy(css = "button[title=Login]")
    WebElement loginSubmitButton;

    @FindBy(css = "div.buttons-set > a")
    WebElement createAnAccount;

    @FindBy(css = "li.error-msg > ul > li > span")
    WebElement alertInvalidInputs;

    @Step("Validar Login header")
    public void validateLoginHeader() {
        loginHeader.isDisplayed();
    }

    @Step("Validar opción Create an Account")
    public void validateCreateAccountButton() {
        createAnAccount.isDisplayed();
    }

    @Step("Validar opción Create an Account")
    public void validateRequiredFields() {
        requiredFields.isDisplayed();
        email.isDisplayed();
        password.isDisplayed();
    }

    @Step("Ingresar email")
    public void ingresarEmail(String emailParam) {
        email.sendKeys(emailParam);
    }

    @Step("Ingresar email")
    public void ingresarPassword(String passwordParam) {
        password.sendKeys(passwordParam);
    }

    @Step("Login Submit")
    public void setLoginSubmitButton() {
        loginSubmitButton.click();
    }

    @Step("Create an account")
    public CreateAccountPage setCreateAnAccount() {
        createAnAccount.click();
        return new CreateAccountPage(driver);
    }

    @Step("Validar mensaje de error por credenciales invalidas")
    public void validateInputsError() {
        alertInvalidInputs.isDisplayed();
    }

}
