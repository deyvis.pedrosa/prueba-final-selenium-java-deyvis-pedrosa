package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import static org.testng.Assert.assertEquals;

public class CartPage {
    private static WebDriver driver;

    public CartPage(WebDriver driver){
        CartPage.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @Step("Validar item en el carro de compras")
    public void validateProductAtCart(String product) {
        assertEquals(driver.findElement(By.linkText(product)).getText(), product);
    }

    @Step("Validar color")
    public void validateItemColor(String color) {
        WebElement itemColor = driver.findElement(By.cssSelector("tr.first.last.odd > td.product-cart-info > dl > dd:nth-child(2)"));
        assertEquals(itemColor.getText(), color);
    }

    @Step("Validar talle")
    public void validateItemSize(String size) {
        WebElement itemColor = driver.findElement(By.cssSelector("tr.first.last.odd > td.product-cart-info > dl > dd:nth-child(4)"));
        assertEquals(itemColor.getText(), size);
    }

}
