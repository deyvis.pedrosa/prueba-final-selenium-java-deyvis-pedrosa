package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CreateAccountPage {
    public static WebDriver driver;

    public CreateAccountPage(WebDriver driver){
        CreateAccountPage.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(css = ".page-title > h1")
    WebElement headerCreateAccount;

    @FindBy(className = "form-instructions")
    WebElement registerInstructions;

    @FindBy(css = "button[title=Register]")
    WebElement submitRegister;

    @FindBy(id = "firstname")
    WebElement firstnameInput;

    @FindBy(id = "lastname")
    WebElement lastnameInput;

    @FindBy(id = "email_address")
    WebElement emailInput;

    @FindBy(id = "password")
    WebElement passwordInput;

    @FindBy(id = "confirmation")
    WebElement passwdConfirmationInput;

    @Step("Validar Create Account header")
    public void validateCreateAccountHeader(){
        headerCreateAccount.isDisplayed();
    }

    @Step("Validar instrucciones de registro")
    public void validateInstructions(){
        registerInstructions.isDisplayed();
    }

    @Step("Ingresar datos")
    public AccountPage setInformationInputs(String firstname, String lastname, String email, String password) {
        firstnameInput.sendKeys(firstname);
        lastnameInput.sendKeys(lastname);
        emailInput.sendKeys(email);
        passwordInput.sendKeys(password);
        passwdConfirmationInput.sendKeys(password);
        submitRegister.click();
        return new AccountPage(driver);
    }
}
