package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.testng.Assert.assertEquals;

public class ProductPage {
    private static WebDriver driver;

    ProductPage(WebDriver driver){
        ProductPage.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(className = "btn-cart")
    WebElement addToCartButton;

    @FindBy(css = "div.product-name > span")
    WebElement productName ;

    @FindBy(className = "price")
    WebElement price;

    @FindBy(id = "attribute92")
    WebElement colorSelection;

    @FindBy(id = "attribute180")
    WebElement sizeSelection;

    @Step("Obtener nombre del producto")
    public String getProductName() { return productName.getText(); }

    @Step("Obtener precio")
    public String getPrice(){
        return price.getText();
    }

    @Step("Clickear 'Add to cart'")
    public CartPage addToCart() {
        addToCartButton.click();
        return new CartPage(driver);
    }

    @Step("Validar producto")
    public void validateProductName(String productParam) {
        assertEquals(getProductName(), productParam);
    }

    @Step("Validar precio")
    public void validatePrice(String priceParam) {
        //WebDriver driver = new ChromeDriver();
        WebDriverWait wait = new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOf(price));
        assertEquals(getPrice(), priceParam);
    }

    @Step("Elegir color del producto")
    public void selectProductColor(String colorParam) {
        Select color = new Select(colorSelection);
        color.selectByVisibleText(colorParam);
    }

    @Step("Elegir talle del producto")
    public void selectProductSize(String sizeParam) {
        Select size = new Select(sizeSelection);
        size.selectByVisibleText(sizeParam);
    }

}