package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import static org.testng.Assert.assertEquals;

public class AccountPage {
    public static WebDriver driver;

    public AccountPage(WebDriver driver){
        AccountPage.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(css = ".page-title > h1")
    WebElement headerCreateAccount;

    @FindBy(css = "li.success-msg > ul > li > span")
    WebElement successAlert;

    @FindBy(css = "p.hello > strong")
    WebElement userWelcome;

    @FindBy(linkText = "ACCOUNT INFORMATION")
    WebElement accountInfoHeader;

    @FindBy(css = "a[href*=\"header-account\"]")
    WebElement headerAccount;

    @Step("Validar Registro")
    public void validateRegistration(String firstnameParam, String lastnameParam) {
        headerCreateAccount.isDisplayed();
        successAlert.isDisplayed();
        assertEquals(userWelcome.getText(),"Hello, "+firstnameParam+" "+lastnameParam+"!");
        accountInfoHeader.isDisplayed();
    }

    @Step("Logout submit")
    public void selectLogout(){
        headerAccount.click();
        WebElement logout_btn = driver.findElement(By.cssSelector("#header-account > div > ul > li.last > a"));
        logout_btn.click();
    }
}
