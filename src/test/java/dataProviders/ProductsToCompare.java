package dataProviders;

import org.testng.annotations.DataProvider;

public class ProductsToCompare {
    @DataProvider(name = "productsToCompare")
    public static Object[][] getProductsToCompare() {
        return new Object[][]{
                {"16GB", "8GB"},
        };
    }
}
