package dataProviders;

import org.testng.annotations.DataProvider;

public class NewUserCredentials {
    @DataProvider(name = "credentials")
    public static Object[][] getNewUserCredentials() {
        return new Object[][]{
                {"Nombre", "Apellido", "random743464@mail.com", "clavedeacceso"},
                {"Nombres", "Apellidos", "random836453@mail.com", "claveacceso"},
        };
    }
}
