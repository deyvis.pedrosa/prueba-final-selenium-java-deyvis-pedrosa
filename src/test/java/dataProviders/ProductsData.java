
package dataProviders;

import org.testng.annotations.DataProvider;

public class ProductsData {
    @DataProvider(name = "products")
    public static Object[][] getProductsData() {
        return new Object[][]{
                {"RETRO CHIC EYEGLASSES", "295,00 $", "German"},
        };
    }
}